package plugins.adufour.filtering;

import icy.sequence.Sequence;

public interface IKernel
{
	/**
	 * @return Creates a sequence with the internal data of this kernel
	 */
	Sequence toSequence();
	
	/**
	 * @return Returns a reference to the internal data of this kernel
	 */
	double[] getData();
}
